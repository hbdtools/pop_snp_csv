import csv

with open('nature_schizo.csv', 'rb') as csvfile, open('formatted_nature_schizo.tsv', 'w+') as outfile:

    reader = csv.DictReader(csvfile)
    for line in reader:

        SNP_name, chromosome, nucleotide_position, p_value_comma = line['indexSNP'], line['CHR'], line['BP'], line['P (GWAS)'] 
        p_value = p_value_comma.replace(',', '.')

        A1, A2 = line['A1A2'][0], line['A1A2'][1]
        frq_cases_A1, frq_controls_A1 = line['FRQ_A1 (cases)'], line['FRQ_A1 (con)']
        
        print SNP_name, chromosome, nucleotide_position, A1, A2, frq_cases_A1, frq_controls_A1

        if frq_cases_A1 > frq_controls_A1:
            allele_increasing_phenotype, allele_decreasing_phenotype = A1, A2
        else:
            allele_increasing_phenotype, allele_decreasing_phenotype = A2, A1

        outline = '\t'.join([SNP_name, chromosome, nucleotide_position, allele_increasing_phenotype, allele_decreasing_phenotype, p_value])

        outfile.write(outline + '\n')
