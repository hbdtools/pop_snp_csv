# README #

Popmatrix is a part of the hbtools toolkit. It is the first part of the Piffer pipeline.

It takes in a list of SNPs. It outputs a .csv file of rows with SNPs, columns with populations and the frequency of each trait increasing allele for each population.

The input file contains the following columns:

SNP name, chromosome, nucleotide position, allele increasing phenotype, allele decreasing phenotype, p value. 

Example input:

```
#!csv

rs56873913	19	50091199	T	G	2.188E-007
rs6670165	1	177280121	T	C	1.156E-007
...
```

Example output:

```
#!csv
SNP_NAME,AFR,AMR,ASN,EUR,SAN
3_63833050_C,0.676248,0.370317,0.531746,0.40159,0.370143
5_140143664_C,0.310893,0.564841,0.526786,0.532803,0.519427
...
```


### Requirements ###

* [Tabix](http://sourceforge.net/projects/samtools/files/tabix/)
* [vcftools](http://vcftools.sourceforge.net/)

### Todo ###

* Add support for [Alfred](http://alfred.med.yale.edu/) format files. This would need some way of automatically downloading SNPs in LD with those found in the Alfred database; one possible way is getting LD data from UCSC.
* Make it possible to choose which population groups to include from 1000 genomes (AFR, ASN, EUR etc.)

### Contribution guidelines ###

* Test the code and raise issues.
* Refactor.
* Add functionality (see todo)